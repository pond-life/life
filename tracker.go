package life

import "sync"

type tracker struct {
	mu        sync.RWMutex
	livingMap map[int]map[int]struct{}
	count     int
}

func (t *tracker) Set(location Location) (_ bool) {
	t.mu.Lock()
	defer t.mu.Unlock()

	if _, ok := t.livingMap[location.Y]; !ok {
		t.livingMap[location.Y] = make(map[int]struct{})
	}
	if _, ok := t.livingMap[location.Y][location.X]; !ok {
		t.livingMap[location.Y][location.X] = struct{}{}
		t.count++
		return true
	}

	return
}

func (t *tracker) Remove(location Location) (_ bool) {
	t.mu.Lock()
	defer t.mu.Unlock()

	if _, ok := t.livingMap[location.Y]; ok {
		if _, ok = t.livingMap[location.Y][location.X]; ok {
			delete(t.livingMap[location.Y], location.X)
			t.count--
			return true
		}
	}

	return
}

func (t *tracker) Test(location Location) (found bool) {
	t.mu.RLock()
	defer t.mu.RUnlock()

	if _, ok := t.livingMap[location.Y]; ok {
		_, found = t.livingMap[location.Y][location.X]
	}

	return
}

func (t *tracker) GetAll() []Location {
	t.mu.RLock()
	defer t.mu.RUnlock()

	all := make([]Location, 0, t.count)
	for y := range t.livingMap {
		for x := range t.livingMap[y] {
			// all = append(all, col)
			all = append(all, Location{X: x, Y: y})
		}
	}

	return all
}

func (t *tracker) Count() int {
	return t.count
}

func (t *tracker) Equals(rhs *tracker) bool {
	if t.Count() != rhs.Count() {
		return false
	}

	for _, loc := range t.GetAll() {
		if !rhs.Test(loc) {
			return false
		}
	}

	return true
}

func (t *tracker) Clone() *tracker {
	shadow := newTracker()

	for _, loc := range t.GetAll() {
		shadow.Set(loc)
	}

	return shadow
}

func newTracker() *tracker {
	t := new(tracker)

	t.livingMap = make(map[int]map[int]struct{})

	return t
}

// vim: set foldmethod=marker:
