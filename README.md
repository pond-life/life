# life  [![GoDoc](http://godoc.org/git.sr.ht/~hokiegeek/life?status.png)](http://godoc.org/git.sr.ht/~hokiegeek/life)

This library allows you to run a [Conway's Game of Life](http://www.conwaylife.com/wiki/Conway%27s_Game_of_Life) simulation.

Related packages:

- [biologist](https://git.sr.ht/~hokiegeek/biologist)
- [biologist-web](https://git.sr.ht/~hokiegeek/biologist-web)
